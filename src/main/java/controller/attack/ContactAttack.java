package controller.attack;

import model.Character;

public class ContactAttack extends AttackDecorator {

    public ContactAttack(final Character c1, final Character c2) {
        super(c1, c2, Damage.CONTACT);
    }

    /**
     * The way the collision is detected depends on the type of the attack.
     */
    @Override
    public void hit() {
        if (!AttackController.collisionsControlerRight(c1, c2) || !AttackController.collisionsControlerLeft(c1, c2)) {
            c2.getBar().updateHealth(this.getDamage());
        }
    }

}
