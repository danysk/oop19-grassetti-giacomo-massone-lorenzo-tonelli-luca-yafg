package controller.attack;

import model.Character;

public abstract class AttackDecorator implements Attack {
    /**
     *This enum define the damage of every attack.
     *
     */
    public static enum Damage {
        LIGHT(5), STRONG(15), CONTACT(1);

        private final int damageValue;

        private Damage(final int damageValue) {
            this.damageValue = damageValue;
        }

        public int getValue(final Damage d) {
            return damageValue;
        }
    }

    protected Character c1;
    protected Character c2;
    protected int damage;

    public AttackDecorator(final Character c1, final Character c2, final Damage type) {
        this.c1 = c1;
        this.c2 = c2;
        this.damage = type.getValue(type);
    }

    @Override
    public abstract void hit();

    public final int getDamage() {
        return this.damage;
    }

}
