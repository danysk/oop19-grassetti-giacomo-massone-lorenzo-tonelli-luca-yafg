/**
 * A class representing the utility methods necessary to opportunely manage the drawing of sprites and the update of
 * characters'coordinates.
 * @author Lorenzo Massone
 * @author Luca Tonelli
 * @version 1.1
 */
package controller;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import model.Character;
import model.Floor;
import model.MediaSound;
import model.Pair;
import model.MediaSound.Sounds;

public class MatchController {
    private CharacterView view;
    private final Floor floor;
    private boolean runGame = true;

    /**
     * This enum define the right frame that the CharacterView have to draw.
     *
     */
    public static enum MOVESET {
        Light, Strong, Walk, Idle, Jump, Dead
    }

    public MatchController(final CharacterView view, final Floor floor) {
        this.view = view;
        this.floor = floor;
    }

    /**
     * This constant is used to adjust the collision when the character jump.
     */
    public static final int COLLISION_JUMP = 50;
    /**
     * This constant is used to adjust the collision when a character collide with
     * another character.
     */
    public static final int COLLISION_BOUNDS = 125;

    private int lightAtkIndex = 0;
    private int strongAtkIndex = 0;
    private boolean stopLightAtk = true;
    private boolean stopStrongAtk = true;

    /**
     * This Service is used to slow down the light attack animation.
     */
    private Service<Void> waitLightAtkTime = new Service<Void>() {
        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                protected Void call() throws Exception {
                    stopLightAtk = false;
                    try {
                        // la sleep va dentro il try
                        Thread.sleep(40);
                    } catch (InterruptedException interrupted) {
                        if (isCancelled()) {
                            // per permettere che questo Thread venga rilanciato eventualmente
                            stopLightAtk = true;
                            updateMessage("Cancelled");
                        }
                    }
                    stopLightAtk = true;
                    return null;
                }
            };
        }
    };

    /**
     * This Service is used to slow down the strong attack animation.
     */
    private Service<Void> waitStrongAtkTime = new Service<Void>() {
        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                protected Void call() throws Exception {
                    stopStrongAtk = false;
                    try {
                        // la sleep va dentro il try
                        Thread.sleep(40);

                    } catch (InterruptedException interrupted) {
                        if (isCancelled()) {
                            // per permettere che questo Thread venga rilanciato eventualmente
                            stopStrongAtk = true;
                            updateMessage("Cancelled");
                        }
                    }
                    stopStrongAtk = true;
                    return null;
                }
            };
        }
    };

    /**
     * Updates the current position of the characters based on current speed and on
     * elapsed time using the flags in Character.
     * 
     * @param c1
     *                           is the current player
     * @param c2
     *                           is the enemy player
     * @param elapsedSeconds
     *                           since the previous call of the method
     */
    public void updateCharacterPosition(final Character c1, final Character c2, final double elapsedSeconds) {
        int horiz = 0;
        int vert = 0;

        if (c1.getX() == c2.getX() && c1.getY() == c2.getY() && c1.getX() == 0) {
            c2.setX(c2.getX() + c2.getImageWidth() - MatchController.COLLISION_BOUNDS);
        }
        if (c1.getX() == c2.getX() && c1.getY() == c2.getY() && c1.getX() == this.floor.getWidth() - (c1.getImageWidth() / 1.5)) {
            c2.setX(c2.getX() - c2.getImageWidth() + MatchController.COLLISION_BOUNDS);
        }

        if (c1.isLeft() && this.collisionsControlerLeft(c1, c2)) {
            horiz = horiz - 3;
        }
        if (c1.isRight() && this.collisionsControlerRight(c1, c2)) {
            horiz = horiz + 3;
        }
        c1.setX(this.clamp(c1.getX() + horiz * c1.getSpeed() * elapsedSeconds, 0,
                this.floor.getWidth() - (c1.getImageWidth() / 1.5)));

        if (c1.isUp() && !c1.getJumpStatement()) {
            MediaSound.Sounds.JUMP.playSound(Sounds.JUMP);
            c1.setJumpStatement(true);
        }

        if (c1.getJumpStatement()) {
            vert -= c1.getJumpSpeed();
            c1.setJumpSpeed(c1.getJumpSpeed() - 0.35);
        }
        if (c1.getY() >= Floor.FLOOR_LINE - c1.getImageHeight()) {
            c1.setJumpSpeed(c1.getDefSpeedJump());
            c1.setJumpStatement(false);
            c1.setY(Floor.FLOOR_LINE - c1.getImageHeight());
        }

        if (!c1.isUp() && !c1.getJumpStatement() && c1.getY() <= Floor.FLOOR_LINE - c1.getImageHeight()) {
            c1.setY(Floor.FLOOR_LINE - c1.getImageHeight());
        }
        c1.setY(this.clamp(c1.getY() + vert * c1.getSpeed() * elapsedSeconds, 0, Floor.FLOOR_LINE - c1.getImageHeight()));

        if (c1.getBar().isDead()) {
            MediaSound.Sounds.DEATH.playSound(Sounds.DEATH);
            this.runGame = false;
        }

    }

    /**
     * A method to draw the right frame according to the current action the player
     * is doing.
     * 
     * @param elapsedSeconds
     *                           seconds passed from last main cycle
     * @param player
     *                           that will be drawn
     * @throws FileNotFoundException
     *                                   when images not found
     */
    public void drawPlayer(final double elapsedSeconds, final Character player) throws FileNotFoundException {
        List<Double> li = new LinkedList<>();
        double x = player.getX();
        double y = player.getY();
        Optional<Integer> index = Optional.of(0);
        MOVESET value = MOVESET.Idle;
        if (player.getBar().isDead()) {
            value = MOVESET.Dead;
        } else if (player.isLightAttacking()) {
            index = Optional.of(lightAtkIndex);
            value = MOVESET.Light;
            if (this.stopLightAtk) {
                this.lightAtkIndex++;
                this.waitLightAtkTime.restart();
            }
            if (this.lightAtkIndex >= view.getAnimationSize(MOVESET.Light)) {
                this.lightAtkIndex = 0;
                player.setLightAttacking(false);
                value = MOVESET.Idle;
            }
        } else if (player.isStrongAttacking()) {
            index = Optional.of(strongAtkIndex);
            value = MOVESET.Strong;
            if (this.stopStrongAtk) {
                this.strongAtkIndex++;
                this.waitStrongAtkTime.restart();
            }
            if (this.strongAtkIndex >= view.getAnimationSize(MOVESET.Strong)) {
                this.strongAtkIndex = 0;
                player.setStrongAttacking(false);
                value = MOVESET.Idle;
            }
        } else if (player.isUp()) {
            value = MOVESET.Jump;
        } else if ((player.isRight() || player.isLeft()) && !player.getJumpStatement()) {
            value = MOVESET.Walk;
        } else {
            value = MOVESET.Idle;
        }
        li = view.update(value, new Pair<Double, Double>(x, y), player.isOrientation(), elapsedSeconds, index);
        player.setImageHeight(li.get(0));
        player.setImageWidth(li.get(1));

    }

    /**
     * This method control the right collision of a character
     * 
     * @param c1
     *               current player
     * @param c2
     *               enemy player
     * @return false if it isn't in collision
     */
    public final boolean collisionsControlerRight(final Character c1, final Character c2) {
        if (this.determinateFirstCharacterPosition(c1.getX(), c2.getX())) {
            if (c1.getX() + c1.getImageWidth() - MatchController.COLLISION_BOUNDS > c2.getX() && c1.isRight()
                    && this.collisionsControlerJump(c1, c2) && this.collisionsControlerUnder(c1, c2)) {
                return false;
            }
            return true;
        }
        return true;
    }

    /**
     * This method control the left collision of a character
     * 
     * @param c1
     *               current player
     * @param c2
     *               enemy player
     * @return false if it isn't in collision
     */
    public final boolean collisionsControlerLeft(final Character c1, final Character c2) {
        if (this.determinateFirstCharacterPosition(c2.getX(), c1.getX())) {
            if (c1.getX() + MatchController.COLLISION_BOUNDS < c2.getX() + c2.getImageWidth() && c1.isLeft()
                    && this.collisionsControlerJump(c1, c2) && this.collisionsControlerUnder(c1, c2)) {
                return false;
            }
            return true;
        }
        return true;
    }

    /**
     * This method control the jumping collision of a character
     * 
     * @param c1
     *               current player
     * @param c2
     *               enemy player
     * @return false if it isn't in collision
     */
    public final boolean collisionsControlerJump(final Character c1, final Character c2) {

        if (c1.getY() - MatchController.COLLISION_BOUNDS - MatchController.COLLISION_JUMP + c1.getImageHeight() < c2.getY()
                && c1.getJumpStatement()) {
            return false;

        }
        return true;

    }

    /**
     * This method control the collision of a character that go under a jumping
     * character
     * 
     * @param c1
     *               current player
     * @param c2
     *               enemy player
     * @return false if it isn't in collision
     */
    public final boolean collisionsControlerUnder(final Character c1, final Character c2) {

        if (c2.getY() - MatchController.COLLISION_BOUNDS + c1.getImageHeight() < c1.getY() && c2.getJumpStatement()) {
            return false;

        }
        return true;

    }

    /**
     * This method determinate the character that is in the left
     * 
     * @param x1
     *               x of character 1
     * @param x2
     *               x of character 2
     * @return true if character 1 is in the left of character 2
     */
    public final boolean determinateFirstCharacterPosition(final double x1, final double x2) {
        return (x1 > x2) ? false : true;
    }

    /**
     * A method that returns the current coordinate ( x or y ) of the character if
     * it's between the edges of the Background, otherwise the minimum or max
     * position allowed.
     * 
     * @param value
     *                     is the current coordinate that is "clamped"
     * @param minValue
     *                     is the minimum value allowed
     * @param maxValue
     *                     is the maximum value allowed
     * @return the correct value
     */
    public double clamp(final double value, final double minValue, final double maxValue) {
        if (value < minValue) {
            return minValue;
        }
        if (value > maxValue) {
            return maxValue;
        }
        return value;
    }

    /**
     * A method to check if the game is running.
     * 
     * @return true if it's running, false otherwise
     */
    public boolean isRunGame() {
        return runGame;
    }

}
