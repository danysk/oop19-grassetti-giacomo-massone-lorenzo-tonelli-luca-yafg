/**
 * this class serves as a controller for the main menu
 */
package controller;

import java.util.HashMap;
import java.util.Map;
import com.sun.javafx.collections.ObservableMapWrapper;
import javafx.application.Platform;
import javafx.collections.ObservableMap;
import javafx.stage.Stage;
import view.factory.GameViewFactory;

public class MainMenuController {

    public MainMenuController(GameViewFactory factory, Stage stage) {
        Map<String, Runnable> map = new HashMap<>();
        map.put("New Game", () -> new CharacterChooserController(factory, stage));
        map.put("Exit", () -> {
            Platform.exit();
            System.exit(0);
        });
        ObservableMap<String, Runnable> entries = new ObservableMapWrapper<>(map);
        factory.getMainMenu(entries);
    }
}
