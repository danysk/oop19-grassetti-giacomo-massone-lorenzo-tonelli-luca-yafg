/**
 * This interface represent a factory which provides all the views needed for the game.
 */
package view.factory;

import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableMap;
import javafx.scene.image.Image;
import javafx.util.Pair;
//import view.gamefield.Sprite;
import view.gameview.GameView;

public interface GameViewFactory {

    /**
     * sets the view to the main menu.
     * 
     * @param entries
     *                    of the MainMenu
     * @return the {@link GameView} representing the main menu
     */
    GameView getMainMenu(ObservableMap<String, Runnable> entries);

    /**
     * sets the view to the character chooser menu.
     * 
     * @param characters
     *                       the list of characters present in a game
     * @param chosen
     *                       an ObjectProperty<String> containing the id of the
     *                       chosen character
     * @return the {@link GameView} representing the character chooser menu
     */
    GameView getCharacterChooser(List<Pair<String, Image>> characters, ObjectProperty<String> chosen);
}
