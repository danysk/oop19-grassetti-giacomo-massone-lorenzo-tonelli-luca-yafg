/**
 * This interface represents the base view of the game. 
 */
package view.gameview;

public interface GameView {

    /**
     * all the layouts needed for the game.
     */
    enum ViewLayouts {
        MAIN_MENU,
        CHARACTER_CHOOSER,
    }

    /**
     * a method that can be used to update the view.
     */
    void update();
}
