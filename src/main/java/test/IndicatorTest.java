package test;
/**
 * @author Giacomo Grassetti
 */
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import model.Indicator;

class IndicatorTest {

    @Test
    void testHealthCapacity() {
        Indicator bar = new Indicator(true);
        Assertions.assertEquals(100, bar.getHealthCapacity());
    }

    @Test
    void testFuryCapacity() {
        Indicator bar = new Indicator(true);
        Assertions.assertEquals(0, bar.getFuryCapacity());
    }

    @Test
    void testUpdateHealth() {
        Indicator bar = new Indicator(true);
        bar.setHealthCapacity(50);
        Assertions.assertEquals(50, bar.getHealthCapacity());
        bar.updateHealth(20);
        Assertions.assertEquals(30, bar.getHealthCapacity());
    }

    @Test
    void testSetFury() {
        Indicator bar = new Indicator(true);
        bar.setFuryCapacity(80);
        Assertions.assertEquals(80, bar.getFuryCapacity());
        bar.setFuryCapacity(20);
        Assertions.assertEquals(100, bar.getFuryCapacity());
    }

    @Test
    void testResetFury() {
        Indicator bar = new Indicator(true);
        bar.setFuryCapacity(80);
        Assertions.assertEquals(80, bar.getFuryCapacity());
        bar.resetFury();
        Assertions.assertEquals(0, bar.getFuryCapacity());
    }

}
